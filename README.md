# Marsrover Kata with Polymorphism in Java
Done on 29/03/2019 with Iván Pérez at IES Estació, Ontinyent. 
Exposed and taught by Fran Ferri.

## Parts accomplished of [the marsrover kata](http://kata-log.rocks/mars-rover-kata)

* You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.
* The rover receives a character array of commands.
* Implement commands that move the rover forward/backward (f,b).
* Implement commands that turn the rover left/right (l,r).