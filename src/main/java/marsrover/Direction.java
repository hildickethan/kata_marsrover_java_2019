package marsrover;

public interface Direction {
	public Location goForwards(Location location);
	
	public Location goBackward(Location location);
	
	public Direction turnLeft();
	
	public Direction turnRight();
}
