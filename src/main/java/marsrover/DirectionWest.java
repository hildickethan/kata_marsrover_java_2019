package marsrover;

public class DirectionWest implements Direction {

	@Override
	public Location goForwards(Location location) {
		location.decreaseX();
		return location;
	}

	@Override
	public Location goBackward(Location location) {
		location.increaseX();
		return location;
	}

	@Override
	public Direction turnLeft() {
		return new DirectionSouth();
	}

	@Override
	public Direction turnRight() {
		return new DirectionNorth();
	}

}
