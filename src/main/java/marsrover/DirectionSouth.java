package marsrover;

public class DirectionSouth implements Direction{

	@Override
	public Location goForwards(Location location) {
		location.decreaseY();
		return location;
	}

	@Override
	public Location goBackward(Location location) {
		location.increaseY();
		return location;
	}

	@Override
	public Direction turnLeft() {
		return new DirectionEast();
	}

	@Override
	public Direction turnRight() {
		return new DirectionWest();
	}


}
