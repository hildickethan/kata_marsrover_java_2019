package marsrover;

public class Rover {

	private Location location;
	private Direction direction;
	
	public Rover() {
		this.location = new Location();
		location.setX(0);
		location.setY(0);
		this.direction = new DirectionNorth();
	}

//	public int getX() {
//		return x;
//	}
//
//	public void setX(int x) {
//		this.x = x;
//	}
//
//	public int getY() {
//		return y;
//	}
//
//	public void setY(int y) {
//		this.y = y;
//	}
/*
	public char getDirection() {
		return direction;
	}

	public void setDirection(char direction) {
		this.direction = direction;
	}

	public void goForwards() {
		switch(this.direction) {
		case 'N':
			y++;
			break;
		case 'S':
			y--;
			break;
		case 'E':
			x++;
			break;
		case 'W':
			x--;
			break;
		default:
			
			break;
		}
	}

	public void goBackwards() {
		switch(this.direction) {
		case 'N':
			y--;
			break;
		case 'S':
			y++;
			break;
		case 'E':
			x--;
			break;
		case 'W':
			x++;
			break;
		default:
			
			break;
		}
	}

	public void turnLeft() {
		switch(this.direction) {
		case 'N':
			direction = 'W';
			break;
		case 'S':
			direction = 'E';
			break;
		case 'E':
			direction = 'N';
			break;
		case 'W':
			direction = 'S';
			break;
		default:
			
			break;
		}
		
	}
	
	public void turnRight() {
		switch(this.direction) {
		case 'N':
			direction = 'E';
			break;
		case 'S':
			direction = 'W';
			break;
		case 'E':
			direction = 'S';
			break;
		case 'W':
			direction = 'N';
			break;
		default:
			
			break;
		}
		
	}*/

	public void run(char[] commands) {
		for (int i = 0; i < commands.length; i++) {
			switch(commands[i]) {
			case 'f':
				this.location = direction.goForwards(location);
				break;
			case 'b':
				this.location = direction.goBackward(location);
				break;
			case 'l':
				this.direction = direction.turnLeft();
				break;
			case 'r':
				this.direction = direction.turnRight();
				break;
			default:
				
				break;
			}
		}
		
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "Rover [location=" + location.getX() + ',' + location.getY() + ", direction=" + direction.getClass().getSimpleName() + "]";
	}

	
	
	
	
}
