package marsrover;

public class DirectionEast implements Direction {

	@Override
	public Location goForwards(Location location) {
		location.increaseX();
		return location;
	}

	@Override
	public Location goBackward(Location location) {
		location.decreaseX();
		return location;
	}

	@Override
	public Direction turnLeft() {
		return new DirectionNorth();
	}

	@Override
	public Direction turnRight() {
		return new DirectionSouth();
	}


}
