package marsrover;

public class DirectionNorth implements Direction {

	@Override
	public Location goForwards(Location location) {
		location.increaseY();
		return location;
	}

	@Override
	public Location goBackward(Location location) {
		location.decreaseY();
		return location;
	}

	@Override
	public Direction turnLeft() {
		return new DirectionWest();
	}

	@Override
	public Direction turnRight() {
		return new DirectionEast();
	}



}
