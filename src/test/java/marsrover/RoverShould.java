package marsrover;

import org.junit.Test;
import org.junit.Assert;

public class RoverShould {

	@Test
	public void be_at_starting_point() {

		// given
		Rover rover = new Rover();
		// when
		
		// then
		Assert.assertEquals(0, rover.getLocation().getX());
		Assert.assertEquals(0, rover.getLocation().getY());
	}
	
	@Test
	public void start_in_direction() {
		Rover rover = new Rover();
		
		Assert.assertEquals(rover.getDirection().getClass().getSimpleName(), "DirectionNorth");
	}
	
	@Test
	public void go_forward() {
		Rover rover = new Rover();
		char[] commands = {'f'};
		rover.run(commands);
		
		Assert.assertEquals(0, rover.getLocation().getX());
		Assert.assertEquals(1, rover.getLocation().getY());
	}

	@Test
	public void go_backward() {
		Rover rover = new Rover();
		char[] commands = {'b'};
		rover.run(commands);
		
		Assert.assertEquals(0, rover.getLocation().getX());
		Assert.assertEquals(-1, rover.getLocation().getY());
	}
	
	@Test
	public void turn_left() {
		Rover rover = new Rover();
		char[] commands = {'l'};
		rover.run(commands);
		
		
		Assert.assertEquals(rover.getDirection().getClass().getSimpleName(), "DirectionWest");
	}
	
	@Test
	public void turn_right() {
		Rover rover = new Rover();
		char[] commands = {'r'};
		rover.run(commands);
		
		Assert.assertEquals(rover.getDirection().getClass().getSimpleName(), "DirectionEast");
	}
	
	@Test
	public void receive_array_of_commands() {
		Rover rover = new Rover();
		char[] commands = {'f','f','b','l','b'};
		
		rover.run(commands);
		
		System.out.println(rover.toString());
//		Assert.assertEquals(1, rover.getX());
//		Assert.assertEquals(1, rover.getY());
//		Assert.assertEquals('E', rover.getDirection());
	}
}
